# Basic Simple Website
This is a basic website using the ElevenTy static site generator and will be the building blocks for both my ElevenTy SSG baseline and design system baseline.

## CSS/Sass Structure
This folder consists of the primary style folders to hold Sass files., the default global Sass file, and the global CSS site file.

### Base
Holds the boilerplate code for the project. Including standard styles such as resets, typographic rules, and brand colors which are commonly used throughout the project.

* accessability
* box sizing
* breakpoints
* clear fix
* colors
* font smoothing
* reset
* typography

### Components
Holds all of the styles for buttons, heros, carousels, sliders, and similar page components or widgets using explicite class naming. The project will typically contain a lot of component files — as the whole site/app should be mostly composed of small modules.

* accordion
* breadcrumb
* buttons
* card
* carosel
* form
* hero
* menu
* modal
* notification
* pagination
* promo strip
* sliders
* table

### Elements
Holds unclassed HTML elements. Including standard styles for items such as H1-H6, lists, links, and images used throughout the project.

* figure
* headings
* hr
* icons
* images
* input
* links
* lists
* media
  
### Layout
Uses OOCSS design patterns. Contains all styles involved with the layout of the project. Such as styles for the header, footer, navigation, grid system, with no cosmetics, and uses agnostic class naming.

* article
* container
* header
* footer
* main
* navigation
* section
* sidebar
* wrapper


### Pages
Any styles specific to individual pages will sit here. For example it’s not uncommon for the home page to require page specific styles that no other page receives.

* about us
* article
* article list
* contact
* index
* service
* service list
* work
* work list

### Theme
This is likely not used in many areas of the project. It would hold files that create project area/page specific overrides, helpers, or utilities. For example if sections contain alternate color schemes, sizing, or layout. Usually effects one piece of the DOM.

* theme
* colors
* full width
* hidden
* margin
* sticky

### Utilities
Gloabally available settings, configuration files, tools, helper files, variables, functions, and mixins. These files are meant to be just helpers which don’t output any CSS when compiled.
                       
* functions
* global settings
* mixins
* variables


